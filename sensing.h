#ifndef sensing_h
#define sensing_h

#include <math.h>
/*********************************************************************************
 **  update_average_temperature()
 **
 **  Function calculates temperature based on value measured via analog pin.
 **
 **  Input:  ADC pin
 **  Output: temperature
 *********************************************************************************/
float update_average_temperature(int analogPin);


/*******************************************************************************
 **  sample_temperature
 **
 **  read temperature using thermistor in voltage devider
 **
 **  Input:  analog pin, bParameter from Steinh-Hard Equation
 **  Output: float temperaure
 *********************************************************************************/
float sample_temperature(uint_least8_t analogPin);

/*********************************************************************************
 **  increaseRPMCounter
 **
 **  Function is called when interrupt is triggered on digital pin 3 by a rising
 **  signal and increase the fan RPM counter
 **
 **  Input:  None
 **  Output: None
 *********************************************************************************/
void increaseRPMCounter();

#endif
