#ifndef globals_h
#define globals_h

//globals---------------------------------------------------------------------
volatile uint_fast32_t totalFlowCount_ = 0;
volatile uint_fast32_t totalRpmCount_ = 0;
volatile uint_fast8_t fan1_speed_ = 0; // form 0 - 255

#endif
